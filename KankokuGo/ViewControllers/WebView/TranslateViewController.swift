//
//  TranslateViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/09/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class TranslateViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    var lop: String = ""
    var type: String = "dich"
    var hour: String = ""
    var list_title: [DataPosts] = []
    var txtTitle: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "MỤC LỤC \(self.txtTitle)"
//        self.showButtonRight(img: "ic_menu_black_24dp")
        self.tableView.register(UINib(nibName: "TitleViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        DispatchQueue.main.async {
            self.getDataTerm("\(self.type)/\(self.lop)/\(self.hour)")
        }
        self.bottomSpace.constant = 0
        if self.lop == "lop5" {
            self.bottomSpace.constant = 80
            self.showAds()
        }
    }
}

extension TranslateViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list_title.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TitleViewCell {
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
            cell.backgroundColor = UIColor.clear
            cell.txtTitle.text = self.nilString(self.list_title[indexPath.item].post_title)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if Defaults[.point]! > 0 {
            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: WebViewController.self)
            webVC.txtTitle = self.nilString(list_title[indexPath.row].post_title)
            webVC.link = list_title[indexPath.row].post_content ?? ""
            self.navigationController?.pushViewController(webVC, animated: true)
        }else {
            self.showAlertActionSheet("Bạn vui lòng kiếm xu để học bài")
        }
    }
}

extension TranslateViewController {
    func getData(_ term_id: NSNumber, _ page: Int) {
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.get_data_term_post("\(term_id)?page=\(page)") { (result) in
            self.hud.dismiss(animated: true)
            if (result.result.isFailure) {
                //                self.showAlerClose(MNConstant.thongbao, "Đang bảo trì hệ thống xin hãy quay lại sau.", closeHandler: nil)
            }else {
                var check: Bool = true
                if let value = result.result.value {
                    if let code = value.code, code == 200 {
                        check = false
                        if let data_obj = value.data, let datas = data_obj.posts?.data {
                            for data in datas {
                                self.list_title.append(data)
                            }
                            if self.nilNumber(data_obj.posts?.last_page).intValue > page {
                                self.getData(term_id, page + 1)
                            }
                        }
                        self.tableView.reloadData()
                    }
                    if check {
                        var message = ""
                        if let msg = value.message {
                            message = msg
                        }else if let msg = value.data?.message{
                            message = msg
                        }
                    }
                }
            }
        }
    }
    
    func getDataTerm(_ str: String) {
        self.hud.show(in: self.view)
        NetworkManager.sharedManager.get_data_term(str) { (result) in
            self.hud.dismiss(animated: true)
            if (result.result.isFailure) {

            }else {
                var check: Bool = true
                if let value = result.result.value {
                    if let code = value.code, code == 200 {
                        check = false
                        if let data_obj = value.data {
                            print(data_obj)
                            if let terms = data_obj.terms, let data = terms.data, data.count > 0, let term_id = data[0].term_id {
                                DispatchQueue.main.async {
                                    self.getData(term_id, 1)
                                }
                            }
                        }
                    }
                    if check {
                        var message = ""
                        if let msg = value.message {
                            message = msg
                        }else if let msg = value.data?.message{
                            message = msg
                        }
                    }
                }
            }
        }
    }
}


