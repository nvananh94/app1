//
//  LessionViewController.swift
//  AKSwiftSlideMenu
//
//  Created by MAC-186 on 4/8/16.
//  Copyright © 2016 Kode. All rights reserved.
//
//

import UIKit
import Parchment

class LessionViewController: BaseViewController {
    @IBOutlet weak var viewContent: UIView!
    var index_pop: Int?
    var is_view_translate: Bool = true
    let translate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(ofType: TranslateViewController.self)
    let grammar = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(ofType: GrammarViewController.self)
    var lop_hoc: String = ""
    var hour: String = ""
    var txtTitle: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.showButtonRight(img: "ic_menu_black_24dp")
        self.navigationItem.title = "MỤC LỤC \(self.txtTitle)"
        self.showAds()
        var controllers : [UIViewController] = []
        translate.title = "DỊCH"
        translate.lop = self.lop_hoc
        translate.hour = self.hour
        controllers.append(translate)
        grammar.title = "NGỮ PHÁP"
        grammar.lop = self.lop_hoc
        grammar.hour = self.hour
        controllers.append(grammar)
        
        let pagingViewController = FixedPagingViewController(viewControllers: controllers)
        self.addChildViewController(pagingViewController)
        self.viewContent.addSubview(pagingViewController.view)
        self.viewContent.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParentViewController: self)
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.indicatorColor = UIColor.red
        pagingViewController.backgroundColor = UIColor().navigationColor()
        pagingViewController.selectedBackgroundColor = UIColor().navigationColor()
        pagingViewController.indicatorOptions = .visible(
            height: 2,
            zIndex: Int.max,
            spacing: .zero,
            insets: .zero
        )
    }
}
