//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import FBSDKShareKit
import JGProgressHUD
import GoogleMobileAds
import SystemConfiguration
import SwiftyUserDefaults

class BaseViewController: UIViewController, SlideMenuDelegate, GADBannerViewDelegate, HandleViewVideo, GADRewardBasedVideoAdDelegate {
    var hud = JGProgressHUD(style: .dark)
    var banner: GADBannerView!
    var rewardBasedVideo: GADRewardBasedVideoAd?
    var adRequestInProgress = false
    let view_xu = Bundle.main.loadNibNamed("KiemXuHocBaiView", owner: self, options: nil)?.first as! KiemXuHocBaiView

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isConnectedToNetwork()
        self.showButtonLeft(img: "ic_nav_back")
        self.showButtonRight(img: "ic_menu_black_24dp")
        self.checkPoint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func checkPoint() {
        if Defaults[.point] == nil {
            Defaults[.point] = 10
        }
    }
    
    func showButtonLeft(img: String) {
        self.navigationItem.leftBarButtonItem = nil
        let leftButton = UIBarButtonItem(image: UIImage(named: img), style: .plain, target: self, action: #selector(self.handleLeftBar))
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
    }
    
    func showButtonRight(img: String) {
        self.navigationItem.rightBarButtonItems = nil
        let leftButton = UIBarButtonItem(image: UIImage(named: img), style: .plain, target: self, action: #selector(onSlideMenuButtonPressed(_:)))
        self.navigationItem.rightBarButtonItem = leftButton
    }

    @objc func handleLeftBar(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAds(){
        banner = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(banner)
        banner.adUnitID = Constant.ads_banner // id con that
        banner.rootViewController = self
        banner.load(GADRequest())
        banner.delegate = self
        self.banner.alpha = 0
        
        
        rewardBasedVideo = GADRewardBasedVideoAd.sharedInstance()
        rewardBasedVideo?.delegate = self
        if !adRequestInProgress && rewardBasedVideo?.isReady == false {
            self.loadAdsXu()
        }
    }

    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
        bannerView.heightAnchor.constraint(equalToConstant: 50).isActive = true

        var topSafeAreaHeight: CGFloat = 0
        var bottomSafeAreaHeight: CGFloat = 0
        
            let window = UIApplication.shared.windows[0]
        if #available(iOS 11.0, *) {
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            topSafeAreaHeight = safeFrame.minY
            bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        } else {
            let safeFrame = window.layoutMarginsGuide.layoutFrame
            topSafeAreaHeight = safeFrame.minY
            bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        }

        view_xu.handleDelegate = self
        view_xu.frame = CGRect(x: 0, y: self.view.frame.height - topSafeAreaHeight - bottomSafeAreaHeight - 115 , width: self.view.frame.width, height: 33)
        view_xu.translatesAutoresizingMaskIntoConstraints = true
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
        self.view.addSubview(view_xu)
        view_xu.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        view_xu.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        view_xu.bottomAnchor.constraint(equalTo: bannerView.topAnchor, constant: -2).isActive = true
        view_xu.heightAnchor.constraint(equalToConstant: 33).isActive = true
    }
    
    func handleViewADS() {
        if !adRequestInProgress && rewardBasedVideo?.isReady == false {
            self.loadAdsXu()
        }
        if rewardBasedVideo?.isReady == true {
            rewardBasedVideo?.present(fromRootViewController: self)
        }
    }
    
    // banner setup
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        self.banner.alpha = 1
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        self.banner.alpha = 0
    }
    // banner end setup
    
    // MARK: GADRewardBasedVideoAdDelegate implementation
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        adRequestInProgress = false
        print("Reward based video ad failed to load: \(error.localizedDescription)")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        adRequestInProgress = false
        print("Reward based video ad is received.")
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
        if !adRequestInProgress && rewardBasedVideo?.isReady == false {
            self.loadAdsXu()
        }
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        Defaults[.point]! += 10
        self.showAlertActionSheet("Bạn đã được cộng 10 xu để học bài!")
        view_xu.txtXu.text = "\(Defaults[.point]!) XU"
    }
    
    func loadAdsXu() {
        rewardBasedVideo?.load(GADRequest(), withAdUnitID: Constant.ads_add_xu)
        adRequestInProgress = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        switch(index){
        case 0:
            self.openViewControllerBasedOnIdentifier("HomeViewController")
            break
        case 1:
//            faceBookShare()
            let textToShare = String(describing: "Chia sẻ ứng dụng của bạn \(Constant.url_app)")
            // URL, and or, and image to share with other apps
            guard let myAppURLToShare = URL(string: Constant.url_app), let image = UIImage(named: "LOGO60") else {
                return
            }
            let items = [textToShare, myAppURLToShare, image] as [Any]
            let avc = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            //Apps to exclude sharing to
            avc.excludedActivityTypes = [
                .airDrop, .print, .saveToCameraRoll, .addToReadingList, .mail, .message,
                .postToFacebook, .postToFlickr, .postToTwitter, .postToTencentWeibo
            ]
            //If user on iPad
            if UIDevice.current.userInterfaceIdiom == .pad {
                if avc.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    avc.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                }
            }
            //Present the shareView on iPhone
            self.present(avc, animated: true, completion: nil)
        case 2:
            let url_fb = Constant.url_facebook
            let appURL = "fb://"
            if url_fb != "" {
                self.openURL(url_fb, appURL)
            }
            break
        case 3:
            let url_fb = Constant.url_youtube
            if url_fb != "" {
                self.openURL(url_fb, "")
            }
            break
        default:
            print("default\n", terminator: "")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        if let wd = UIApplication.shared.delegate?.window {
            wd!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }

//        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
//
//        let topViewController : UIViewController = self.navigationController!.topViewController!
        
//        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
//            print("Same VC")
//        } else {
//            self.navigationController!.pushViewController(destViewController, animated: true)
//        }
    }
    
    func faceBookShare(){
        let content = FBSDKShareLinkContent()
        content.contentURL =  URL(string: Constant.url_app)
        let dialog : FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = FBSDKShareDialogMode.automatic
        dialog.show()
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = 1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
//        navigationController?.setNavigationBarHidden(true, animated: true)
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
    
    func animateView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    
    func showAlertActionSheet(_ str: String ) {
        let importantAlert: UIAlertController = UIAlertController(title: str, message:"", preferredStyle: .actionSheet)
        self.present(importantAlert, animated: true, completion: nil)
        let delay = 1.5 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            importantAlert.dismiss(animated: true, completion: nil)
        }

    }
    
    func showAlerYesNo(title: String, message: String?, yesActionHandler handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Cập nhật", style: .default, handler: handler)
        let closeAction = UIAlertAction(title: "Không", style: .default, handler: nil)
        alert.addAction(closeAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlerClose(_ title: String, _ message: String?, closeHandler handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Đóng", style: .cancel, handler: handler)
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isConnectedToNetwork() {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            self.showAlerClose("Thông báo", "Mạng không ổn định, vui lòng kiểm tra lại kết nối của bạn !", closeHandler: nil)
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        print("isReachable: ----> ", isReachable)
        print("needsConnection: ----> ", needsConnection)
        if !(isReachable && !needsConnection) {
            self.showAlerClose("Thông báo", "Mạng không ổn định, vui lòng kiểm tra lại kết nối của bạn !", closeHandler: nil)
            return
        }
    }
}

extension BaseViewController { /* Check nil value
     *************************************************/
    func openURL(_ url: String, _ app: String){
        guard let url = URL(string: url) else {
            return
        }
        if app != "", let app = URL(string: app) {
            if UIApplication.shared.canOpenURL(app) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(app, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(app)
                }
            }else {
                //redirect to safari because the user doesn't have Instagram
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else {
            //redirect to safari because the user doesn't have Instagram
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func nilString(_ value: String?) -> String{
        if let v = value {
            return v
        }
        return ""
    }
    
    func nilNumber(_ value: NSNumber?) -> NSNumber{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilInt(_ value: Int?) -> Int{
        if let n = value {
            return n
        }
        return 0
    }
    
    func nilFloat(_ value: Float?) -> Float{
        if let n = value {
            return n
        }
        return 0
    }
}
