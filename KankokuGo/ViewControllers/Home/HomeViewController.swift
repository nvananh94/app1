//
//  HomeViewController.swift
//  KankokuGo
//
//  Created by tld on 3/27/19.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMobileAds

class HomeViewController: BaseViewController, GADInterstitialDelegate, UIAlertViewDelegate {
    var interstitial: GADInterstitial!

    @IBOutlet weak var collectionView: UICollectionView!
    var arrayDataClass = [Dictionary<String,String>]()
    let column: CGFloat = 2
    let minItemSpacingIphone: CGFloat = 20.0
    let minItemSpacingIpad: CGFloat = 30.0
    let leftItemSpacingIphone: CGFloat = 20.0
    let leftItemSpacingIpad: CGFloat = 30.0
    var totalWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = nil
        setupDataClass()
        self.showAds()
        createAndLoadInterstitial()
    }
    
    fileprivate func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: Constant.ads_full) // id con that
        interstitial.delegate = self
        interstitial.load(DFPRequest())
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        VersionCheck.shared.checkAppStore() { isNew, version in
            if isNew! {
                self.showAlerYesNo(title: "Thông báo", message: "Đã có phiên bản cập nhật mới, bạn muốn cập nhật không?", yesActionHandler: { (alert) in
                    let url_fb = Constant.url_app
                    if url_fb != "" {
                        self.openURL(url_fb, "")
                    }
                })
            }
        }
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupDataClass() {
        arrayDataClass.append(["name":"Lớp 1 초급 1", "image":"class1", "title":"LỚP 1", "class": "lop1", "hour": "0h"])
        arrayDataClass.append(["name":"Lớp 2 초급 2", "image":"class2", "title":"LỚP 2", "class": "lop2", "hour": "0h"])
        arrayDataClass.append(["name":"Lớp 3 초급 3", "image":"class3", "title":"LỚP 3", "class": "lop3", "hour": "0h"])
        arrayDataClass.append(["name":"Lớp 4 초급 4", "image":"class4", "title":"LỚP 4", "class": "lop4", "hour": "0h"])
        arrayDataClass.append(["name":"Lớp 5 (50h) 심화", "image":"class5_2", "title":"LỚP 5","class": "lop5", "hour": "50h"])
        arrayDataClass.append(["name":"Lớp 5 (20h) 심화", "image":"class5_1", "title":"LỚP 5", "class": "lop5", "hour": "20h"])
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        if UIDevice.isPad() {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIpad) - (leftItemSpacingIpad * 2)
        } else {
            totalWidth = boundWidth - ((column - 1) * minItemSpacingIphone) - (leftItemSpacingIphone * 2)
        }
        return totalWidth / column
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("count", arrayDataClass.count)
        return arrayDataClass.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(ofType: ClassCollectionCell.self, for: indexPath) {
            cell.nameLabel?.text = arrayDataClass[indexPath.row]["name"] ?? ""
            cell.imageView?.image = UIImage(named: arrayDataClass[indexPath.row]["image"] ?? "")
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: itemWidth, height: (228*itemWidth)/134)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lop_hoc = arrayDataClass[indexPath.row]["class"] ?? ""
        let hour = arrayDataClass[indexPath.row]["hour"] ?? "0h"
        let title = arrayDataClass[indexPath.row]["title"] ?? ""
        if lop_hoc == "lop5" {
            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: TranslateViewController.self)
            webVC.lop = lop_hoc
            webVC.hour = hour
            webVC.txtTitle = title
            self.navigationController?.pushViewController(webVC, animated: true)
        }else {
            let webVC = self.instantiateViewController(fromStoryboard: .main, ofType: LessionViewController.self)
            webVC.lop_hoc = lop_hoc
            webVC.hour = hour
            webVC.txtTitle = title
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if UIDevice.isPad() {
            return UIEdgeInsets(top: leftItemSpacingIpad, left: leftItemSpacingIpad, bottom: leftItemSpacingIpad, right: leftItemSpacingIpad)
        } else {
            return UIEdgeInsets(top: leftItemSpacingIphone, left: leftItemSpacingIphone, bottom: leftItemSpacingIphone, right: leftItemSpacingIphone)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.isPad() {
            return minItemSpacingIpad
        } else {
            return minItemSpacingIphone
        }
    }
    
}

class VersionCheck { // tự động kiểm tra phiên bản trên appstore
    public static let shared = VersionCheck()
    var newVersionAvailable: Bool?
    var appStoreVersion: String?
    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?)->Void)? = nil) {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            if let json = response.result.value as? NSDictionary,
                let results = json["results"] as? NSArray,
                let entry = results.firstObject as? NSDictionary,
                let appVersion = entry["version"] as? String,
                let ourVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            {
                isNew = ourVersion != appVersion
                versionStr = appVersion
            }
            self.appStoreVersion = versionStr
            self.newVersionAvailable = isNew
            callback?(isNew, versionStr)
        }
    }
}


class MNNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let themeColor: UIColor = UIColor().navigationColor()
        UINavigationBar.appearance().barTintColor = themeColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        UIBarButtonItem.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().backgroundColor = UIColor().navigationColor()
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = UIColor.white
        UITabBar.appearance().barTintColor = UIColor().navigationColor()
    }
}
