//
//  LauchScreenViewController.swift
//  KankokuGo
//
//  Created by Nguyen Van Anh on 04/10/2019.
//  Copyright © 2019 tld. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LauchScreenViewController: UIViewController, GADInterstitialDelegate, UIAlertViewDelegate {
    var interstitial: GADInterstitial!

    override func viewDidLoad() {
        super.viewDidLoad()

        createAndLoadInterstitial()
    }
    
    fileprivate func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910") // id con test
//        interstitial = GADInterstitial(adUnitID: "ca-app-pub-2138414238273550/8806574795") // id con that
        interstitial.delegate = self
        interstitial.load(DFPRequest())
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        if let wd = UIApplication.shared.delegate?.window {
              wd!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        }
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
    }
}
